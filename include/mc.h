/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/* Monte Carlo move step in Yao et al. and Rowley et al. */
void integrateMC(struct Atom atoms[], int n);

/* different Metropolis tests in Yao et al. */
int metropolis_yao(struct Atom atoms[], double Ubefore, double Uafter,
                   int count, int m, int flag_move, double Z, double V);

/* Monte Carlo in Yao et al. */
int montecarloStep_yao(struct Atom atoms[], int *index, int count, int *part_indx,
                       int *type_move, double Z, double V);

/* different Metropolis tests in Rowley et al. */
int metropolis_rowley(struct Atom atoms[], double Ubefore, double Uafter,
                      int count, int m, int flag_move, double Z, double V);

/* Monte Carlo in Rowley et al. */
int montecarloStep_rowley(struct Atom atoms[], int count, int *part_indx,
                          int *type_move, double Z, double V);

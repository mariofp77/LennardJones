/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/* function to calculate greatest common divisor */
int gcd(int a, int b);

/* function to set up cubic lattice */
double latticex, latticey, latticez;
void makeLatticePosition(double a);

/* function for gaussian random variables */
double gaussian();

/* function to initialize the system */
void initialize(struct Atom atoms[]);

/* Verlet integration step */
void integrateStep(struct Atom atoms[]);

/* function for uniform random variables */
int uniform(int n);

//AMB: Write system snapshot to PDB, labeling real/ghost as C/H atoms.
void write_to_PDB(struct Atom atoms[],int tindex);

/* integration and measurement */
void run();

/* properties of the system */
int     met;         /* method used */
int     M;           /* maximum number of particles */
int     N;           /* number of particles */
double  rho;         /* density of the system */
double  mu;          /* chemical potential of the system */
double  L;           /* length of the box */
double  dt;          /* duration of single time step */
double  halfDt;      /* half the time step */
long int numSteps;   /* how long to run */
int     screen;      /* frequency for printing */
int     logfile;     /* frequency for writing in the log file */
unsigned long int seed;
double  burninTime;  /* when to start measuring energies etc. */
double  K;           /* kinetic energy */
double  U;           /* potential energy */
double  H;           /* total energy */
double  T;           /* kinetic temperature */
double  temp;        /* instantaneous temperature */
double  beta;        /* beta for the thermodynamic temperature */
double  pres;        /* instantaneous pressure */
double  vir;         /* virial */
double  pr_mv;       /* probability of moving */
double  pr_in;       /* probability of insertion */
double  pr_de;       /* probability of deletion */
double  pr_inde;     /* probability of insertion/deletion */
FILE    *pdb;        /* PDB file for the trajectory */
gsl_rng *g_rng;      /* global variable for the PRNG */
const gsl_rng_type *rng_t; /* constant with the type for the PRNG */
/* HMC/GHMC */
int     iL;          /* length of the MD trajectories */
int     il;          /* length of the slow-growth trajectories */
double  phi;         /* angle used in the momentum update */
double  phi;         /* angle used in the momentum update */
/* HMC/GHMC */
char    tcutoff;     /* type of cut-off used */

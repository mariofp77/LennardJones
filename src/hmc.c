/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "struc.h"
#include "global.h"
#include "rand.h"

/* store the atoms data before the Monte Carlo */
void store_atoms(struct Atom atoms[], struct Atom atoms_local[])
{
   int i;
   for ( i = 0; i < M; i++ )
      atoms_local[i] = atoms[i];
}

/* Monte Carlo for HMC */
void montecarloStep(struct Atom atoms[], int *index, int *part_indx, int *rand_indx, int *flag_move)
{
   int result; /* result of the Metropolis test */
   int n, m; /* particle indexes */
   double pr;
   
   pr = gsl_rng_uniform(g_rng); /* generate a probability */

   if (pr < pr_mv) // Movement
   {
     *flag_move = 2;
   }
   else
   {
     if (pr < pr_in && N < M) // Insertion
     {
        *flag_move = 1;
        /* select the particle */
        n = uniform(M-N-1); // a number is selected among the number of ghost particles
        m = index[N+n]; // the corresponding index is found in the array of indexes
        atoms[m].g =1;
     }
     else if (pr >= pr_in && N > 0) // Deletion
     {
        *flag_move = 0;
        /* select the particle */
        n = uniform(N-1); // a number is selected among the number of real particles
        m = index[n]; // the corresponding index is found in the array of indexes
        atoms[m].g =0;
     }
   *rand_indx = n;
   *part_indx = m;
   }
}

/* Metropolis test for HMC */
int metropolis(struct Atom atoms[], double Ubefore, double Kbefore,
               double Uafter, double Kafter, int count, int m, int flag_move,
               double Z, double V)
{
    int result;
    double r, Pr, dH;

    r = gsl_rng_uniform(g_rng);
    /* difference in energy */
    dH = (Kafter + Uafter) - (Kbefore + Ubefore);
    Pr = exp(-beta*dH);

    if (flag_move == 1) /* insertion */
    {
       if (Z*V*Pr/(N+1) > r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Difference in Kinetic = %f and in Potential = %f\n",Kafter - Kbefore,
                                                                         Uafter - Ubefore);
             printf("Difference in Hamiltonian = %f\n",dH);
             printf("Metropolis: We accept %f with probability %f\n",Z*V*Pr/(N+1),r);
          }
          result = 1; /* the new state is accepted */
          N = N+1; /* increase the number of particles */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Difference in Kinetic = %f and in Potential = %f\n",Kafter - Kbefore,
                                                                         Uafter - Ubefore);
             printf("Difference in Hamiltonian = %f\n",dH);
             printf("Metropolis: We reject %f with probability %f\n",Z*V*Pr/(N+1),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 0; /* the particle is kept ghost */
       }
    }
    else if (flag_move == 0) /* deletion */
    {
       if (N*Pr/(Z*V) >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Difference in Kinetic = %f and in Potential = %f\n",Kafter - Kbefore,
                                                                         Uafter - Ubefore);
             printf("Difference in Hamiltonian = %f\n",dH);
             printf("Metropolis: We accept %f with probability %f\n",N*Pr/(Z*V),r);
          }
          result = 1; /* the new state is accepted */
          N = N-1; /* one particle is removed */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Difference in Kinetic = %f and in Potential = %f\n",Kafter - Kbefore,
                                                                         Uafter - Ubefore);
             printf("Difference in Hamiltonian = %f\n",dH);
             printf("Metropolis: We reject %f with probability %f\n",N*Pr/(Z*V),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 1; /* the particle is kept real */
       }
    }
    else if (flag_move == 2) /* move */
    {
       if (Pr >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("Difference in Kinetic = %f and in Potential = %f\n",Kafter - Kbefore,
                                                                         Uafter - Ubefore);
             printf("Difference in Hamiltonian = %f\n",dH);
             printf("Metropolis: We accept %f with probability %f\n",Pr,r);
          }
          result = 1; /* the new state is accepted */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("Difference in Kinetic = %f and in Potential = %f\n",Kafter - Kbefore,
                                                                         Uafter - Ubefore);
             printf("Difference in Hamiltonian = %f\n",dH);
             printf("Metropolis: We reject %f with probability %f\n",Pr,r);
          }
          result = 0; /* the new state is rejected */
       }
    }

    return result;
}

/* generate velocities at the Maxwell-Boltzmann distribution */
double maxwellBoltzmann()
{
   double scale, y;
   
   scale = sqrt(T); // this is the standard deviation of the normal
   
   y = gsl_ran_gaussian_ziggurat(g_rng, scale);
   
   return y;
}

/* momentum update for HMC */
void generate_momenta(struct Atom atoms[])
{
   int i;

   K = 0;

   for ( i = 0; i < M; i = i + 1 )
   {
      atoms[i].px = maxwellBoltzmann();
      atoms[i].py = maxwellBoltzmann();
      atoms[i].pz = maxwellBoltzmann();
      K = K
         + atoms[i].g*(atoms[i].px*atoms[i].px
         + atoms[i].py*atoms[i].py
         + atoms[i].pz*atoms[i].pz);
   }

   /* compute also instantaneous kinetic temperature and energy */
   temp = K/(3*N);
   K = K/2;
}

/* generate noise for the momentum update */
double noise()
{
   double scale, y;
   
   scale = sqrt(T); // this is the standard deviation of the normal
   
   y = gsl_ran_gaussian_ziggurat(g_rng, scale);
   
   return y;
}

/* momentum update for GHMC */
void momentum_update(struct Atom atoms[])
{ 
   double u[M][3];
   int i;

   K = 0;

   for ( i = 0; i < M; i = i + 1 )
   {
      atoms[i].px = cos(phi)*atoms[i].px + sin(phi)*noise();
      u[i][0] = -sin(phi)*atoms[i].px + cos(phi)*noise();
      atoms[i].py = cos(phi)*atoms[i].py + sin(phi)*noise();
      u[i][1] = -sin(phi)*atoms[i].py + cos(phi)*noise();
      atoms[i].pz = cos(phi)*atoms[i].pz + sin(phi)*noise();
      u[i][2] = -sin(phi)*atoms[i].pz + cos(phi)*noise();
      K = K + atoms[i].g*(atoms[i].px*atoms[i].px
                        + atoms[i].py*atoms[i].py
                        + atoms[i].pz*atoms[i].pz);
   }

   /* compute also instantaneous kinetic temperature and energy */
   temp = K/(3*N);
   K = K/2;
}

/* flip the momenta for GHMC */
void momentum_flip(struct Atom atoms[])
{
   int i;
   
   for ( i = 0; i < M; i = i +1 )
   {
      atoms[i].px = -atoms[i].px;
      atoms[i].py = -atoms[i].py;
      atoms[i].pz = -atoms[i].pz;
   }
}

/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "struc.h"
#include "global.h"
#include "pbc.h"
#include "force.h"
#include "hmc.h"

/* function to compute forces during slow growth */
double computeForcesGrow(struct Atom atoms_sg[])
{
   int     i, j;                      /* particle indices */
   double  dx, dy, dz;                /* distance vector */
   double  r, r2, r2i, r6i;           /* distance, r^2, r^{-2} and r^{-6} */
   double  fij;                       /* force multiplier */
   double  eij;                       /* potential energy between i and j */
   double  x, alpha, dalpha;          /* auxiliar variable in smoothing */
   double  rc;                        /* outer cutoff radius */
   double  rcp;                       /* inner cutoff, or start of smoothing */
   rc = L/2;
   rcp = L/2;

   /* initialize energy and forces to zero */
   double U_sg = 0.0;

   for (i = 0; i < M; i = i + 1) 
   {
      atoms_sg[i].fx = 0;
      atoms_sg[i].fy = 0;
      atoms_sg[i].fz = 0;
   }

   /* determine interaction for each pair of particles (i,j) */
   for (i = 0; i < M-1; i = i + 1)
   {
      for (j = i+1; j < M; j = j + 1)
      {
         /* determine distance in periodic system */
         dx = makePeriodic(atoms_sg[i].rx - atoms_sg[j].rx);
         dy = makePeriodic(atoms_sg[i].ry - atoms_sg[j].ry);
         dz = makePeriodic(atoms_sg[i].rz - atoms_sg[j].rz);
         r2 = dx*dx + dy*dy + dz*dz;

         /* note : using square distance saves taking a sqrt */
         //else if ( r2 < rc*rc ) 
         if (r2 < rc*rc)
         {
            r2i = 1/r2;
            r6i = r2i*r2i*r2i;
            fij = 48*r2i*r6i*(r6i-0.5); // f = (48/r^14 - 24/r^8)
            fij = fij*atoms_sg[i].g*atoms_sg[j].g; /* correction for the ghost particles */
            eij = 4*r6i*(r6i-1); // U = 4(1/r^12 - 1/r^6)
            eij = eij*atoms_sg[i].g*atoms_sg[j].g; /* correction for the ghost particles */

            atoms_sg[i].fx = (atoms_sg[i].fx + fij*dx)*atoms_sg[i].g;
            atoms_sg[i].fy = (atoms_sg[i].fy + fij*dy)*atoms_sg[i].g;
            atoms_sg[i].fz = (atoms_sg[i].fz + fij*dz)*atoms_sg[i].g;
            atoms_sg[j].fx = (atoms_sg[j].fx - fij*dx)*atoms_sg[j].g;
            atoms_sg[j].fy = (atoms_sg[j].fy - fij*dy)*atoms_sg[j].g;
            atoms_sg[j].fz = (atoms_sg[j].fz - fij*dz)*atoms_sg[j].g;
            U_sg = U_sg + eij;
         }
      }
   }

   return U_sg;
}

/* Verlet integration step with the slow growth. One has to keep in mind that the structure atoms and the
   potential energy U has the particle inserted/deleted, the ones correspondings to its previous state are
   atoms_sg and U_sg */
void integrateStepGrow(struct Atom atoms[], struct Atom atoms_sg[], int part_indx, int count)
{
   int i;
   double U_sg;
   double lambda11,lambda21,lambda12,lambda22;

   /* define the parameters of the linear interpolation */
   lambda11 = 1.0 - (double)count/il;
   lambda21 = (double)count/il;

   /* half-force step */
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].px = atoms[i].px + 0.5*dt*(lambda11*atoms_sg[i].fx + lambda21*atoms[i].fx);
      atoms[i].py = atoms[i].py + 0.5*dt*(lambda11*atoms_sg[i].fy + lambda21*atoms[i].fy);
      atoms[i].pz = atoms[i].pz + 0.5*dt*(lambda11*atoms_sg[i].fz + lambda21*atoms[i].fz);
   }

   /* full free motion step */
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].rx = atoms[i].rx + dt*atoms[i].px;
      atoms[i].ry = atoms[i].ry + dt*atoms[i].py;
      atoms[i].rz = atoms[i].rz + dt*atoms[i].pz;
   }

   /* positions were changed, so recompute the forces */
   computeForces(atoms);
   /* store the new positions in the sg structure */
   store_atoms(atoms,atoms_sg);
   /* keep the particle inserted/deleted in its previous state in the sg structure */
   atoms_sg[part_indx].g = 1 - atoms_sg[part_indx].g;
   /* recompute the forces in the sg structure and the sg potential
      energy */
   U_sg = computeForcesGrow(atoms_sg);

   /* define the parameters of the linear interpolation */
   lambda12 = 1.0 - (double)(count+1.0)/il;
   lambda22 = (double)(count+1.0)/il;

   /* final force half-step */
   K = 0;
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].px = atoms[i].px + 0.5*dt*(lambda12*atoms_sg[i].fx + lambda22*atoms[i].fx);
      atoms[i].py = atoms[i].py + 0.5*dt*(lambda12*atoms_sg[i].fy + lambda22*atoms[i].fx);
      atoms[i].pz = atoms[i].pz + 0.5*dt*(lambda12*atoms_sg[i].fz + lambda22*atoms[i].fx);
      K = K 
         + atoms[i].px*atoms[i].px*atoms[i].g
         + atoms[i].py*atoms[i].py*atoms[i].g
         + atoms[i].pz*atoms[i].pz*atoms[i].g;
   }

   /* finish computing T, K, H */
   temp = K/(3*N); // 2/3 of the Kinetic energy
   K = K/2;
   H = lambda12*U_sg + lambda22*U + K;
}

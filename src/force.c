/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <string.h>
#include "struc.h"
#include "global.h"
#include "pbc.h"
#include "sg.h"
#include "lj.h"
#include "ghost.h"
#include "hmc.h"
#include "mc.h"
#include "rand.h"

/* do the cut-off for the forces and the potential energy */
void cutoff(double r2, double rc, double rcp, struct Atom atoms[], int i, int j, double *fij, double *eij)
{
   double r, x, alpha, dalpha;
   double rcp2, rcp2i, rcp6i;

   if (strcmp(&tcutoff,"smooth") == 0 && r2 > rcp*rcp)
   {
      /* replace phi by alpha * phi                         */
      /* based on a slight rewriting of the alpha factor to */
      /*  alpha = 1/2 - 1/4 x (x^2 - 3)                     */
      /* where                                              */
      /*  x = (2 r - rcp - rc)/(rcp - rc)                   */
      r      = sqrt(r2);
      x      = (2*r - rcp - rc)/(rcp - rc);
      alpha  = 0.5 - 0.25*x*(x*x - 3);
      dalpha = 1.5*(x*x - 1)/(r*(rcp-rc)); /* -D[alpha,r]/3 */
      *fij    = alpha*(*fij) + dalpha*(*eij);
      *fij    = *fij*atoms[i].g*atoms[j].g; /* correction for the ghost particles */
      *eij    = alpha*(*eij);
      *eij    = *eij*atoms[i].g*atoms[j].g; /* correction for the ghost particles */
   }
   else if (strcmp(&tcutoff,"shift") == 0)
   {
      rcp2 = rcp*rcp;
      rcp2i = 1/rcp2;
      rcp6i = rcp2i*rcp2i*rcp2i;
      *eij = *eij - 4*rcp6i*(rcp6i-1);
      *eij = *eij*atoms[i].g*atoms[j].g; /* correction for the ghost particles */
   }
}

/* function to compute forces */
void computeForces(struct Atom atoms[])
{
   int     i, j;                      /* particle indices */
   double  dx, dy, dz;                /* distance vector */
   double  r, r2, r2i, r6i;           /* distance, r^2, r^{-2} and r^{-6} */
   double  fij;                       /* force multiplier */
   double  eij;                       /* potential energy between i and j */
   double  vij;                       /* virial multiplier */
   double  x, alpha, dalpha;          /* auxiliar variable in smoothing */
   double  rc;                        /* outer cutoff radius */
   double  rcp;                       /* inner cutoff, or start of smoothing */
   rc = 2.5;
   rcp = 2.5-0.1;

   /* initialize energy, forces and the virial to zero */
   U = 0;

   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].fx = 0;
      atoms[i].fy = 0;
      atoms[i].fz = 0;
   }

   vir = 0;

   /* determine interaction for each pair of particles (i,j) */
   for ( i = 0; i < M-1; i = i + 1 )
   {
      for ( j = i+1; j < M; j = j + 1 ) 
      {
         /* determine distance in periodic system */
         dx = makePeriodic(atoms[i].rx - atoms[j].rx);
         dy = makePeriodic(atoms[i].ry - atoms[j].ry);
         dz = makePeriodic(atoms[i].rz - atoms[j].rz);
         r2 = dx*dx + dy*dy + dz*dz;

         /* note : using square distance saves taking a sqrt */
         if ( r2 < rc*rc ) 
         {
            r2i = 1/r2;
            r6i = r2i*r2i*r2i;
            vij = 48*r6i*(r6i-0.5); // vir = (48/r^12 - 24/r^6)
            fij = r2i*vij; // f = (48/r^14 - 24/r^8)
            fij = fij*atoms[i].g*atoms[j].g; /* correction for the ghost particles */
            eij = 4*r6i*(r6i-1); // U = 4(1/r^12 - 1/r^6)
            eij = eij*atoms[i].g*atoms[j].g; /* correction for the ghost particles */

            cutoff(r2, rc, rcp, atoms, i, j, &fij, &eij);

            /* multiply the forces times the distances d */
            atoms[i].fx = (atoms[i].fx + fij*dx)*atoms[i].g;
            atoms[i].fy = (atoms[i].fy + fij*dy)*atoms[i].g;
            atoms[i].fz = (atoms[i].fz + fij*dz)*atoms[i].g;
            atoms[j].fx = (atoms[j].fx - fij*dx)*atoms[j].g;
            atoms[j].fy = (atoms[j].fy - fij*dy)*atoms[j].g;
            atoms[j].fz = (atoms[j].fz - fij*dz)*atoms[j].g;
            /* compute the virial */
            vir = vir + vij*atoms[i].g*atoms[j].g;
            /* sum the potential energy contributions */
            U = U + eij;
         }
      }
   }
   vir = vir/3;
}

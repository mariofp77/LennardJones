/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "struc.h"
#include "global.h"
#include "rand.h"
#include "force.h"
#include "ghost.h"

/* Monte Carlo move step in Yao et al. and Rowley et al. */
void integrateMC(struct Atom atoms[], int n)
{
   double ix, iy, iz;

   ix = 2*gsl_rng_uniform(g_rng) - 1;
   atoms[n].rx = atoms[n].rx + ix*0.03; /* the displacement is as proposed in Rowley et al. */
   iy = 2*gsl_rng_uniform(g_rng) - 1;
   atoms[n].ry = atoms[n].ry + iy*0.03;
   iz = 2*gsl_rng_uniform(g_rng) - 1;
   atoms[n].rz = atoms[n].rz + iz*0.03;
   /* we do not compute forces here since they are computed in the Monte Carlo */
}

/* different Metropolis tests in Yao et al. */
int metropolis_yao(struct Atom atoms[], double Ubefore, double Uafter,
                   int count, int m, int flag_move, double Z, double V)
{
    int result;
    double r, Pr, dU;

    r = gsl_rng_uniform(g_rng);
    dU = Uafter - Ubefore; /* difference in potential energy */
    Pr = exp(-beta*dU);
    if (flag_move == 1) /* insertion */
    {
       if (Z*V*Pr/(N+1) > r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Metropolis: We accept %f with probability %f\n",Z*V*Pr/(N+1),r);
          }
          result = 1; /* the new state is accepted */
          N = N+1; /* increase the number of particles */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Metropolis: We reject %f with probability %f\n",Z*V*Pr/(N+1),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 0; /* the particle is kept ghost */
          U = Ubefore; /* the potential energy is recovered */
       }
    }
    else if (flag_move == 0) /* deletion */
    {
       if (N*Pr/(Z*V) >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Metropolis: We accept %f with probability %f\n",N*Pr/(Z*V),r);
          }
          result = 1; /* the new state is accepted */
          N = N-1; /* one particle is removed */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Metropolis: We reject %f with probability %f\n",N*Pr/(Z*V),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 1; /* the particle is kept real */
          U = Ubefore; /* the potential energy is recovered */
       }
    }
    else if (flag_move == 2) /* move */
    {
       if (Pr >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("The particle moved is %d. Is it real? %d\n",m,atoms[m].g);
             printf("Metropolis: We accept %f with probability %f\n",Pr,r);
          }
          result = 1; /* the new state is accepted */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("The particle moved is %d. Is it real? %d\n",m,atoms[m].g);
             printf("Metropolis: We reject %f with probability %f\n",Pr,r);
          }
          result = 0; /* the new state is rejected */
          U = Ubefore; /* the potential energy is recovered */
       }
    }

    return result;
}


/* Monte Carlo in Yao et al. */
int montecarloStep_yao(struct Atom atoms[], int *index, int count, int *part_indx,
                       int *type_move, double Z, double V)
{
   int flag_move; /* flag for the different kinds of moves: 0 deletion, 1 insertion, 2 move */
   int result; /* result of the Metropolis test */
   int n, m; /* particle indexes */
   double pr;
   double rx = 0.0, ry = 0.0, rz = 0.0; /* backup for the positions */
   double oldU; /* backup for the potential */
   
   /* generate a probability */
   pr = gsl_rng_uniform(g_rng);

   if (pr < pr_mv) // Movement
   {
     flag_move = 2;
     m = uniform(M-1); /* pick a particle, real or ghost */
     /* store the positions before moving */
     rx = atoms[m].rx;
     ry = atoms[m].ry;
     rz = atoms[m].rz;
     integrateMC(atoms,m);
   }
   else
   {
     if (pr < pr_in && N < M) // Insertion
     {
        flag_move = 1;
        n = uniform(M-N-1); // a number is selected among the number of ghost particles
        m = index[N+n]; // the corresponding index is found in the array of indexes
        atoms[m].g =1;
     }
     else if (pr >= pr_in && N > 0) // Deletion
     {
        flag_move = 0;
        n = uniform(N-1); // a number is selected among the number of real particles
        m = index[n]; // the corresponding index is found in the array of indexes
        atoms[m].g =0;
     }
   }

   oldU = U; /* we store the old potential energy */
   /* number of particles changed, so recompute the forces */
   computeForces(atoms);
   
   /* perform Metropolist test */
   result = metropolis_yao(atoms, oldU, U, count, m, flag_move, Z, V);

   if (result == 1)
   {
      if (count%screen == 0)
         printf("ACCEPTED\n");
      *part_indx = m; /* index of the particle moved */
      *type_move = flag_move; /* type of move */
      /* arrange the index array */
      arrange_index(flag_move,index,n,m);
   }
   else
   {
      if (count%screen == 0)
         printf("REJECTED\n");
      if (flag_move == 2) // in case of rejected movement restore the positions
      {
         atoms[m].rx = rx;
         atoms[m].ry = ry;
         atoms[m].rz = rz;
      }
   }

   H = U + K;

   return result;
}

/* different Metropolis tests in Rowley et al. */
int metropolis_rowley(struct Atom atoms[], double Ubefore, double Uafter,
                      int count, int m, int flag_move, double Z, double V)
{
    int result;
    double r, Pr, dU;

    r = gsl_rng_uniform(g_rng);
    dU = Uafter - Ubefore; /* difference in potential energy */
    Pr = exp(-beta*dU);
    if (flag_move == 1) /* insertion */
    {
       if (Z*V*Pr/(M-N) >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Metropolis: We accept %f with probability %f\n",Z*V*Pr/(M-N),r);
          }
          result = 1; /* the new state is accepted */
          N = N+1; /* increase the number of particles */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Metropolis: We reject %f with probability %f\n",Z*V*Pr/(M-N),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 0; /* the particle is kept ghost */
          U = Ubefore; /* the potential energy is recovered */
       }
    }
    else if (flag_move == 0) /* deletion */
    {
       if ((M-N+1)*Pr/(Z*V) >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Metropolis: We accept %f with probability %f\n",(M-N+1)*Pr/(Z*V),r);
          }
          result = 1; /* the new state is accepted */
          N = N-1; /* one particle is removed */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Metropolis: We reject %f with probability %f\n",(M-N+1)*Pr/(Z*V),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 1; /* the particle is kept real */
          U = Ubefore; /* the pontential energy is recovered */
       }
    }
    else if (flag_move == 2) /* move */
    {
       if (Pr >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("The particle moved is %d\n",m);
             printf("Metropolis: We accept %f with probability %f\n",Pr,r);
          }
          result = 1; /* the new state is accepted */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("The particle moved is %d\n",m);
             printf("Metropolis: We reject %f with probability %f\n",Pr,r);
          }
          result = 0; /* the new state is rejected */
          U = Ubefore; /* the potential energy is recovered */
       }
    }

    return result;
}

/* Monte Carlo in Rowley et al. */
int montecarloStep_rowley(struct Atom atoms[], int count, int *part_indx,
                          int *type_move, double Z, double V)
{
   int flag_move; /* flag for the different kinds of moves: 0 deletion+move, 1 insertion+move, 2 move */
   int result; /* result of the Metropolis test */
   int m; /* particle indexes */
   double pr, pr_mv, pr_inde; /* probabilites of movement, insertion/deletion */
   double rx = 0.0, ry = 0.0, rz = 0.0; /* backup for the positions */
   double oldU; /* backup for the potential */
   
   /* generate a probability */
   pr = gsl_rng_uniform(g_rng);
   m = uniform(M-1); /* pick a particle, real or ghost */

   if (pr < pr_mv) // Movement
   {
     flag_move = 2;
     /* store the positions before moving */
     rx = atoms[m].rx;
     ry = atoms[m].ry;
     rz = atoms[m].rz;
     integrateMC(atoms,m); // The function still has to be defined
   }
   else // Insertion/Deletion
   {
     if (atoms[m].g == 0)
        flag_move = 1;
     else
        flag_move = 0;
     /* store the positions before moving */
     rx = atoms[m].rx;
     ry = atoms[m].ry;
     rz = atoms[m].rz;
     integrateMC(atoms,m); // The particle is moved
     atoms[m].g = 1 - atoms[m].g; // The particle is inserted or deleted
   }

   oldU = U; /* we store the old potential energy */
   /* number of particles changed, so recompute the forces */
   computeForces(atoms);
   
   /* perform Metropolist test */
   result = metropolis_rowley(atoms, oldU, U, count, m, flag_move, Z, V);

   if (result == 1)
   {
      if (count%screen == 0)
         printf("ACCEPTED\n");
      *part_indx = m; /* index of the particle moved */
      *type_move = flag_move; /* type of move */
   }
   else
   {
      if (count%screen == 0)
         printf("REJECTED\n");
      atoms[m].rx = rx;
      atoms[m].ry = ry;
      atoms[m].rz = rz;
   }

   H = U + K;

   return result;
}

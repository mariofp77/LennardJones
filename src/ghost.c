/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "struc.h"
#include "global.h"

/* generate ghost particles */
void generateGhost(struct Atom atoms[])
{
   int i;

   for ( i = 0; i < M; i++ )
   {
       atoms[i].g = ( i < N ? 1 : 0); /* the first N particles are ghost and the rest are real */
   }
}

/* this function moves the indexes in the array if a particle is
   inserted or deleted. This function is only used after acceptance.
   It is important to keep in mind that the number of real particles N
   has been already updated */
void arrange_index(int flag_move, int *index, int n, int m)
{
   int backup;
   
   switch (flag_move)
   {
      /* if there is an insertion the first ghost particle
         index is exchanged with the new inserted particle 
         index */
      case 1: // insertion
         backup = index[N-1]; // the first ghost particle index is stored
         index[N-1] = m; // in its position the index of the new particle is placed
         index[(N-1)+n] = backup; // the backup is moved to the position that had the inserted particle
         break;
      /* if there is a deletion the deleted particle index
         is exchanged with the last real particle index */
      case 0: // deletion
         backup = index[N]; // the last real particle index is stored
         index[N] = m; // in its position the index of the removed particle is placed
         index[n] = backup; // the backup is moved to the position that had the removed particle
         break;
   }
}

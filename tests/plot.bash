#!/bin/bash -x

for FILE in pos*txt; do
    gnuplot --persist <<- EOF
        splot "${FILE}"
EOF
read -n 1 -s
done

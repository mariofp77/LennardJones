# Load trajectory of LJ system:
load trajLJ.pdb

# Hide all lines (whether PyMOL considers the atoms bonded or nonbonded):
hide lines
hide nonbonded

# Show box lines
show lines, resn BOX

# Display LJ atoms as small spheres:
set sphere_scale, 0.08
show spheres, resn LJ

# Color ghost/real atoms as yellow/green (using occupancy field "q"):
#spectrum q, yellow_green
# No, I decided instead to use a different element for each: ghost=H and real=C.

# Orthoscopic view is better to see the box:
set orthoscopic, on

